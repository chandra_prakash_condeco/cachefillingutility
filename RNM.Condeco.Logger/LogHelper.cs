﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNM.Condeco.Logger
{
    public class LogHelper
    {
        bool logInfoFlag = false;
        string logPath = "C:\\Logs";
        string logfileError;
        string logfileInfo;
        public LogHelper()
        {
            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LogPath"]))
                logPath = ConfigurationManager.AppSettings["LogPath"].Trim();


            bool.TryParse(ConfigurationManager.AppSettings["LogInfoFlag"], out logInfoFlag);

            try
            {
                Directory.CreateDirectory(logPath);
                logfileError = logPath + "\\" + "Errors.txt";
                logfileInfo = logPath + "\\" + "Info.txt";
            }
            catch (Exception ex)
            {
                using (StreamWriter sw = File.AppendText("C:\\Logs\\CondecoErrors.txt"))
                {
                    sw.WriteLine("Log path does not exists. " + logPath);
                }
            }
        }

        public void LogInfo(string infoText)
        {
            if (logInfoFlag)
            {
                using (StreamWriter sw = File.AppendText(logfileInfo))
                {
                    sw.WriteLine(DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + "  |  " + infoText);
                }
            }
        }
        public void LogError(string errorText,LogEnums.ErrorLogType errorLogType)
        {
            using (StreamWriter sw = File.AppendText(logfileError))
            {
                sw.WriteLine(DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + "  |  " + errorLogType + "  |  " + errorText);
            }
        }
        public void LogError(string errorText, Exception ex)
        {
            using (StreamWriter sw = File.AppendText(logfileError))
            {
                sw.WriteLine(DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + "  |  " + LogEnums.ErrorLogType.ErrorUnhandled + "  |  " + errorText + "  |  " + ex.Message + "  |  " + ex.StackTrace);
            }
        }
    }
}
