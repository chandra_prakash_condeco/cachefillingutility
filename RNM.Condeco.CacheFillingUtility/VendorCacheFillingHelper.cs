﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RNM.Condeco.CacheFillingUtility
{
    public class VendorCacheFillingHelper
    {
        Logger.LogHelper logger = new Logger.LogHelper();
        public bool FillVendorCache(int userID, string serverList, int vendorCacheDaysPast, int vendorCacheDaysFuture)
        {
            DateTime startDate = DateTime.Now.Date.AddDays(-1* vendorCacheDaysPast -1);
            string[] cacheList = serverList.Split(',');
            try
            {
                int noOfDays = vendorCacheDaysPast + vendorCacheDaysFuture + 2;
                for (int i = 0; i <= noOfDays; i++)
                {
                    string cacheStartDt = startDate.AddDays(i).ToString(@"yyyy-MM-dd\THH:mm:ss\Z", CultureInfo.InvariantCulture);
                    string cacheEndDt = startDate.AddDays(i).AddHours(23).AddMinutes(59).ToString(@"yyyy-MM-dd\THH:mm:ss\Z", CultureInfo.InvariantCulture);

                    foreach (string cacheServer in cacheList)
                    {
                        if (!string.IsNullOrWhiteSpace(cacheServer))
                        {
                            //string url = @"http://condeco32sp1master/Vendors/vendors.svc/?userID=204951&startDate=2016-10-18T00%3A00%3A00.000Z&endDate=2016-10-18T23%3A59%3A59.000Z&_=1475668190944";
                            logger.LogInfo("Vendor Cache filling for date " + cacheStartDt + " and for server " + cacheServer + " started");

                            try
                            {
                                ExecuteRestMethodGET(cacheServer + "/Vendors/vendors.svc/?userID=" + userID + "&startDate=" + cacheStartDt + "&endDate=" + cacheEndDt);
                            }
                            catch
                            {
                                logger.LogInfo("Vendor Cache filling for user " + userID + " date " + cacheStartDt + " and for server " + cacheServer + " NOT completed due to errors. See Error.txt");
                            }
                            logger.LogInfo("Vendor Cache filling for user " + userID + " date " + cacheStartDt + " and for server " + cacheServer + " completed");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Error in method FillVendorCache. class VendorCacheFillingHelper", ex);
            }
            return true;
        }
        private void callAPI(int userID,string cacheStartDt,string cacheEndDt,string cacheServer)
        {
            logger.LogInfo("Vendor Cache filling for date " + cacheStartDt + " and for server " + cacheServer + " started");
            ExecuteRestMethodGET(cacheServer + "/Vendors/vendors.svc/?userID=" + userID + "&startDate=" + cacheStartDt + "&endDate=" + cacheEndDt);
            logger.LogInfo("Vendor Cache filling for user " + userID + " date " + cacheStartDt + " and for server " + cacheServer + " completed");

        }
        public string ExecuteRestMethodGET(string url)
        {
            string jsonResponse = string.Empty;
            try
            {
                using (WebClientEx webClient = new WebClientEx())
                {
                    webClient.Timeout = 10 * 60 * 1000;
                    jsonResponse = webClient.DownloadString(url);
                }

                //    WebRequest req2 = WebRequest.Create(url);
                //req2.Method = "GET";
                //req2.ContentType = @"application/json; charset=utf-8";
                //using (HttpWebResponse response = (HttpWebResponse)req2.GetResponse())
                //{
                //    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                //    {
                //        jsonResponse = sr.ReadToEnd();
                //        //Console.WriteLine(jsonResponse);
                //    }
                //}
            }
            catch (Exception ex)
            {
                logger.LogError("Error in method ExecuteRestMethodGET. class VendorCacheFillingHelper", ex);
                throw ex;
            }
            return jsonResponse;
        }
    }
    public class WebClientEx : WebClient
    {
        public int Timeout { get; set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);
            request.Timeout = Timeout;
            return request;
        }
    }
}
