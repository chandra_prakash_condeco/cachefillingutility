﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNM.Condeco.CacheFillingUtility
{
    class Program
    {
        static Logger.LogHelper logger = new Logger.LogHelper();
        static VendorCacheFillingHelper cacheFillingHelper = new VendorCacheFillingHelper();
        static void Main(string[] args)
        {
            try
            {
                string cacheServers = ConfigurationManager.AppSettings["CacheServerURLs"].ToString();
                //int condecoUserID = 2;
                //int.TryParse(ConfigurationManager.AppSettings["CondecoUserID"], out condecoUserID);
                //condecoUserID = (condecoUserID == 0) ? 2 : condecoUserID;

                int vendorCacheDaysPast = 10;
                int.TryParse(ConfigurationManager.AppSettings["VendorCacheDaysPast"], out vendorCacheDaysPast);
                vendorCacheDaysPast = (vendorCacheDaysPast == 0) ? 10 : vendorCacheDaysPast;

                int vendorCacheDaysFuture = 10;
                int.TryParse(ConfigurationManager.AppSettings["VendorCacheDaysFuture"], out vendorCacheDaysFuture);
                vendorCacheDaysFuture = (vendorCacheDaysFuture == 0) ? 10 : vendorCacheDaysFuture;

                //DateTime startDate = DateTime.Now;
                //DateTime.TryParse(ConfigurationManager.AppSettings["StartDate"], out startDate);

                int condecoUserID = 2;
                string strUserIDs = ConfigurationManager.AppSettings["CondecoUserID"] + "";
                foreach (string strUserID in strUserIDs.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                {
                    condecoUserID = (strUserID == "0") ? 2 : Convert.ToInt32(strUserID);
                    //Task.Factory.StartNew(() => cacheFillingHelper.FillVendorCache(condecoUserID, vendorCacheDays, cacheServers, startDate));
                    cacheFillingHelper.FillVendorCache(condecoUserID, cacheServers,vendorCacheDaysPast,vendorCacheDaysFuture);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Error in method Main(string[] args). class program.cs", ex);

            }

        }

    }
}
